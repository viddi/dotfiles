# Viddi's dot files

Just some stuff tailored for myself.

## Installation

### Powerline fonts

```
git clone https://github.com/powerline/fonts.git
cd fonts
./install.sh
```

### NeoVim

* `brew install python3`
* `pip2 install neovim --upgrade`
* `pip3 install neovim --upgrade`
* `brew install neovim`

* Universal ctags
  * `brew tap universal-ctags/universal-ctags`
  * `brew install --HEAD universal-ctags`
